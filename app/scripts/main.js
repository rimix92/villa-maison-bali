// Uncomment to enable Bootstrap tooltips
// https://getbootstrap.com/docs/4.0/components/tooltips/#example-enable-tooltips-everywhere
// $(function () { $('[data-toggle="tooltip"]').tooltip(); });

// Uncomment to enable Bootstrap popovers
// https://getbootstrap.com/docs/4.0/components/popovers/#example-enable-popovers-everywhere
// $(function () { $('[data-toggle="popover"]').popover(); });

$(document).ready(function() {
		$('.image-chargement').addClass('charge');
		setTimeout(function () {
				$('.image-chargement').addClass('pulse');
		}, 2000);
	// Chargment de la page
	setTimeout(function () {
			$('.loading-table').addClass('slideOutUp');
			setTimeout(function () {
				$('.loading-table').remove();
			}, 1000);
	}, 3000);


		// effet ascenseur
		$('.js-scrollTo').on('click', function() { // Au clic sur un élément
			var page = $(this).attr('href'); // Page cible
			var speed = 750; // Durée de l'animation (en ms)
			$('html, body').animate( { scrollTop: $(page).offset().top}, speed ); // Go
			return false;
		});


// effet scroll

  $(window).scroll(function () {
  	    if ($(window).scrollTop() == 0) {
          $('.navbar').removeClass('block-fixe');
  	   } else {
          $('.navbar').addClass('block-fixe');
        }

  });


  $(window).scroll(function () {
        if ($(window).scrollTop() > 350) {
          $('.fa-angle-down').addClass('transparent');
       } else {
           $('.fa-angle-down').removeClass('transparent');
       }
  });

  $(window).scroll(function () {
        if ($(window).scrollTop() > 100) {
          $('.logo-img').addClass('transparent');
					$('.navbar-brand').removeClass('transparent');
       } else {
           $('.logo-img').removeClass('transparent');
					 $('.navbar-brand').addClass('transparent');
       }
  });

  ScrollReveal().reveal('.headline',{ delay: 100 });
});



const imgContent = document.querySelectorAll('.img-content-hover');

function showImgContent(e) {
  for(var i = 0; i < imgContent.length; i++) {
    imgContent[i].style.left = e.pageX + 'px';
    imgContent[i].style.top = e.pageY + 'px';
  }
};

document.addEventListener('mousemove', showImgContent);
